const overlay = document.createElement("div")
overlay.id = "double-jump-overlay"
overlay.append(document.createElement('span'))
document.body.append(overlay)

let prev, playInterval, overlayTimeout, seek = 0
document.addEventListener("touchend", ev => {
  /** @type {HTMLMediaElement} */
  const el = ev.target
  if(
    el.tagName === "VIDEO"
    && ev.touches.length === 0
    && prev
    && prev.target === el
    && prev.touches.length === 0
    && prev.timeStamp >= ev.timeStamp - 400
  ) {
    const touch = ev.changedTouches.item(0)
    const rect = el.getBoundingClientRect()
    const x = 100 * (touch.clientX - rect.left) / rect.width

    if(x < 33 || x > 66) {
      ev.preventDefault()

      //
      const dir = x > 66
      const diff = 10 * (dir ? 1 : -1)
      el.currentTime += diff

      // Play
      el.play()
      let tries = 0
      const clear = () => clearInterval(playInterval)
      const retry = () => { tries++ < 5 ? el.play().then(clear) : clear() }
      clear()
      playInterval = setInterval(retry, 100)

      // Overlay
      overlay.style.top = (rect.top + 0.5 * rect.height) + "px"
      overlay.style.left = (rect.left + (dir ? 0.75 : 0.25) * rect.width) + "px"

      if(!overlay.classList.contains('show')) {
        seek = diff
        overlay.firstChild.innerText = dir ? '»' : '«'
        overlay.classList.add('show', 'hide')
      } else {
        seek += diff
        overlay.firstChild.innerText = (seek > 0 ? '+' : '') + seek
        overlay.classList.add('delta')
        overlay.classList.remove('hide')
      }

      clearTimeout(overlayTimeout)
      overlayTimeout = setTimeout(() => {
        if(overlay.classList.contains('hide')) {
          overlay.classList.remove('show', 'hide', 'delta')
        } else {
          overlay.classList.add('hide')
          overlayTimeout = setTimeout(() => {
            overlay.classList.remove('show', 'hide', 'delta')
          }, 800)
        }
      }, 800)
    }
  }

  prev = ev
})